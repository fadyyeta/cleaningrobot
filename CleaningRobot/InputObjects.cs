﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleaningRobot
{
    public class InputObject
    {
        public string[][] map { get; set; }
        public Point start { get; set; }
        public string[] commands { get; set; }
        public int battery { get; set; }
    }
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string facing { get; set; }
    }
    public class OutPutObject
    {
        public List<Point> Visited { get; set; }
        public List<Point> Cleaned { get; set; }
        public Point Final { get; set; }
        public int Battery { get; set; }

    }
}
