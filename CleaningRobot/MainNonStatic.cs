﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleaningRobot
{
    public class MainNonStatic
    {
        public string facing;
        public string[] facingDictionary = new string[] { "N", "E", "S", "W" };
        public int lengthY;
        public int lengthX;
        public int battery;
        public InputObject inputObj = new InputObject();
        public int i;
        public Point nextPos = new Point();
        public Point currentPos = new Point();
        public int changeX = 0;
        public int changeY = 0;
        public List<Point> visit = new List<Point>();
        public List<Point> cleaned = new List<Point>();
        public int posX = 0;
        public int posY = 0;
        public int trials = 5;

        public string ReadFile(string sourceFile)
        {
            string dataReturned = "";
            StreamReader sr = new StreamReader(sourceFile);
            //StreamReader sr = new StreamReader(args[0]);
            string data = sr.ReadLine();
            while (data != null)
            {
                dataReturned += data;
                data = sr.ReadLine();
            }
            return dataReturned;
        }
        public void CreateFile(string outputFile, string data)
        {
            TextWriter tw = new StreamWriter(outputFile, true);
            tw.WriteLine(data);
            tw.Close();
            Console.WriteLine("File Created");
            // for debugging uncomment the following line
            //Console.Read();
        }
        public void ChangeFacing(string direction)
        {
            int currentIndex = Array.IndexOf(facingDictionary, facing);
            if (currentIndex == (facingDictionary.Length - 1))
            {
                if (direction == "TR")
                {
                    facing = facingDictionary[0];
                }
                if (direction == "TL")
                {
                    facing = facingDictionary[facingDictionary.Length - 1];
                }

            }
            else if (currentIndex == 0)
            {
                if (direction == "TR")
                {
                    facing = facingDictionary[1];
                }
                if (direction == "TL")
                {
                    facing = facingDictionary[facingDictionary.Length - 1];
                }
            }
            else
            {
                if (direction == "TR")
                {
                    facing = facingDictionary[currentIndex + 1];
                }
                if (direction == "TL")
                {
                    facing = facingDictionary[currentIndex - 1];
                }
            }
            if (facing == "N")
            {
                changeY = -1;
            }
            if (facing == "S")
            {
                changeY = 1;
            }
            if (facing == "E")
            {
                changeX = 1;
            }
            if (facing == "W")
            {
                changeX = -1;
            }
            nextPos = new Point()
            {
                X = posX + changeX,
                Y = posY + changeY,
                facing = facing
            };
        }
        public void BackOff(int trialOrder = 0)
        {
            for (int trial = trialOrder; trial < trials; trial++)
            {
                switch (trial)
                {
                    case 0:
                        CommandTR();
                        CommandA(0);
                        break;
                    case 1:
                        CommandTL();
                        CommandB(1);
                        CommandTR();
                        CommandA(1);
                        break;
                    case 2:
                        CommandTL();
                        CommandTL();
                        CommandA(2);
                        break;
                    case 3:
                        CommandTR();
                        CommandB(3);
                        CommandTR();
                        CommandA(3);
                        break;
                    case 4:
                        CommandTL();
                        CommandTL();
                        CommandA(4);
                        break;
                    default:
                        break;
                }
                trial = trials;
            }
        }
        public void CommandTR()
        {
            if (battery >= 1)
            {
                ChangeFacing("TR");
                battery = battery - 1;
            }
            else
            {
                i = inputObj.commands.Length;
            }
        }
        public void CommandTL()
        {
            if (battery >= 1)
            {
                ChangeFacing("TL");
                battery = battery - 1;
            }
            else
            {
                i = inputObj.commands.Length;
            }
        }
        public void CommandA(int trialOrder = -1)
        {

            nextPos.X = currentPos.X + changeX;
            nextPos.Y = currentPos.Y + changeY;
            if (battery >= 2)
            {
                if (nextPos.Y >= 0 && nextPos.Y <= (lengthY - 1) && nextPos.X >= 0 && nextPos.X <= (lengthX - 1))
                {
                    if (inputObj.map[nextPos.X][nextPos.Y] == "S")
                    {
                        Point currentPos = nextPos;
                        battery = battery - 2;
                        visit.Add(currentPos);
                    }
                    else
                    {
                        BackOff(trialOrder + 1);
                    }
                }
                // to be continued
                else
                {
                    BackOff(trialOrder + 1);
                }
            }
            else
            {
                i = inputObj.commands.Length;
            }
        }
        public void CommandB(int trialOrder = -1)
        {

            nextPos.X = currentPos.X - changeX;
            nextPos.Y = currentPos.Y - changeY;
            if (battery >= 3)
            {
                if (nextPos.Y >= 0 && nextPos.Y <= (lengthY - 1) && nextPos.X >= 0 && nextPos.X <= (lengthX - 1))
                {
                    if (inputObj.map[nextPos.X][nextPos.Y] == "S")
                    {
                        currentPos = nextPos;
                        battery = battery - 3;
                        //visit.Add(currentPos);
                    }

                    else
                    {
                        BackOff(trialOrder + 1);
                    }
                }
                else
                {
                    BackOff(trialOrder + 1);
                }
            }
            else
            {
                i = inputObj.commands.Length;
            }
        }
        public void Main(string[] args)
        {
            // fill string.data with file data
            string data = ReadFile("../../" + args);
            //string data = ReadFile("../../test1.json");

            inputObj = (InputObject)JsonConvert.DeserializeObject(data, typeof(InputObject));

            // our object is now here
            lengthY = inputObj.map.GetLength(0);
            lengthX = inputObj.map[0].GetLength(0);
            posX = inputObj.start.X;
            posY = inputObj.start.Y;
            facing = inputObj.start.facing;
            battery = inputObj.battery;
            string[] commands = inputObj.commands;
            currentPos = new Point()
            {
                X = posX,
                Y = posY,
                facing = facing
            };

            for (i = 0; i < inputObj.commands.Length; i++)
            {
                //ChangeFacing(inputObj.commands[i]);

                switch (commands[i])
                {
                    case "TR":
                        CommandTR();
                        break;
                    case "TL":
                        CommandTL();
                        break;
                    case "A":
                        CommandA();
                        break;
                    case "C":
                        if (battery >= 5)
                        {
                            cleaned.Add(currentPos);
                            battery = battery - 5;
                        }
                        else
                        {
                            i = inputObj.commands.Length;
                        }
                        break;
                    default:
                        break;
                }
            }


            //create file
            OutPutObject outPutObject = new OutPutObject()
            {
                Visited = visit,
                Cleaned = cleaned,
                Final = currentPos,
                Battery = battery
            };

            string output = JsonConvert.SerializeObject(outPutObject, Formatting.Indented);
            //CreateFile("../../test2.json", output);
            CreateFile("../../"+ args[1], output);
        }
    }
}
